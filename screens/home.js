import React from 'react';
import {Dimensions, Image, ScrollView, StyleSheet, Text, View} from 'react-native';

import FeaturedServicesList from '../components/services/FeaturedServicesList';
import ServicesList from '../components/services/ServicesList';
import NewsList from '../components/news/NewsList';

/*Use one of two solution to render svg in application*/
import IconFA from 'react-native-vector-icons/dist/FontAwesome';
import {Location} from '../images/svg';

import globalStyle from '../styles/globalStyle';


const home = () => {

    return (
        <ScrollView>

            <View style={styles.headerImgContainer}>
                <Image resizeMode='contain'
                       style={styles.headerImg}
                       source={require('../images/Header.png')}/>
            </View>

            <View style={styles.container}>

                <Text style={globalStyle.customText}>Good Afternoon Peyman!</Text>

                <View style={styles.headerLocation}>
                    <View style={styles.headerLocationText}>
                        <Location width="15" height="15"></Location>
                        <Text style={{paddingLeft: 5}} >Iran University of medical sciences</Text>
                    </View>
                </View>

                <View style={styles.sectionContainer}>
                    <FeaturedServicesList/>
                </View>

                <View style={[styles.sectionContainer, styles.sectionBorderBottom]}>
                    <ServicesList/>
                </View>

                <View style={[styles.sectionContainer, styles.sectionBorder]}>
                    <NewsList/>
                </View>
            </View>

        </ScrollView>

    );
}

const styles = StyleSheet.create({
    headerImgContainer: {
        position: 'absolute',
        top: 0
    },

    container: {
        paddingVertical: 10
    },

    headerLocation: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    headerLocationText: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(255, 255, 255, 0.58)',
        borderRadius: 10,
        paddingVertical: 5,
        paddingHorizontal: 10
    },

    headerImg: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: (Dimensions.get('window').width) * 0.3586
    },

    sectionContainer:{
        padding: 10,
    },

    sectionBorder:{
        borderWidth: 1,
        borderColor: '#ccc',
        marginTop: 10,
    },

    sectionBorderBottom:{
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
    }
});


const TabIcon = (props) => (
    <IconFA name="home"
            size={25}
            color={props.focused ? '#007AFF' : 'darkgrey'}
    />
)


home.navigationOptions = {
    tabBarIcon: TabIcon
};


export default home;
