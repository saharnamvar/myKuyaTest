import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MapView from 'react-native-maps';

import IconFA from 'react-native-vector-icons/dist/FontAwesome';
import {Location} from '../images/svg';

const map = () => {

    const [region, setRegion] = useState({
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        }
    );

    function onRegionChangeComplete(currentRegion) {
        setRegion(currentRegion);
    }

    return (
        <>
            <MapView
                style={styles.map}
                initialRegion={region}
                loadingEnabled={true}
                loadingIndicatorColor="#666666"
                loadingBackgroundColor="#eeeeee"
                moveOnMarkerPress={false}
                showsUserLocation={true}
                showsCompass={true}
                showsPointsOfInterest={false}
                onRegionChangeComplete={onRegionChangeComplete}
                provider="google">
            </MapView>
            <View style={styles.marker}>
                <Location width="25" height="25"></Location>
            </View>
            <View style={styles.location}>
                <Text style={styles.locationText}>{region.latitude + ", " + region.longitude}</Text>
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    map: {
        flex: 1
    },

    marker: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },

    location: {
        position: 'absolute',
        bottom: 30,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },

    locationText: {
        backgroundColor: 'rgba(255, 255, 255, 0.58)',
        padding: 10,
        borderRadius: 10,
    }
});

const TabIcon = (props) => (
    <IconFA name="map-marker"
            size={25}
            color={props.focused ? '#007AFF' : 'darkgrey'}
    />
)


map.navigationOptions = {
    tabBarIcon: TabIcon
};

export default map;
