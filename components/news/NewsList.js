import React, {useEffect, useState} from 'react';
import {FlatList, Text} from 'react-native';

import globalStyle from '../../styles/globalStyle';
import NewsItem from './NewsItem'

let imgDir = '../../images/';
const newsList = () => {
    const newsListData = [
        {id: '1', title: 'news title1', haedline: 'news content 1 ....', img: require(imgDir + '29.png')},
        {id: '2', title: 'news title2', haedline: 'news content 2 ....', img: require(imgDir + '27.png')},
    ];

    const [newsList, setNewsList] = useState([]);

    useEffect(() => {
        let newsList = getNewsList();
        setNewsList(newsList);
    }, []);

    function getNewsList() {
        return newsListData;
    }

    return (
        <>
            <Text style={globalStyle.sectionHeader}>What's New</Text>
            <FlatList
                data={newsList}
                renderItem={({item}) => <NewsItem item={item}/>}
                keyExtractor={item => item.id}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
            />
        </>
    );
}



export default newsList;
