import React from 'react';
import {Dimensions, Image, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import globalStyle from '../../styles/globalStyle';

const newsList = (props) => {
    return (

        <TouchableOpacity style={[globalStyle.card, styles.container]}>
            <View style={styles.imgContainer}>
                <Image style={{width: '100%', height: '100%'}} resizeMode='cover'
                       source={props.item.img}/>
            </View>
            <View style={styles.titleContainer}>
                <Text style={styles.title}>{props.item.title}</Text>
                <Text style={styles.headline}>{props.item.haedline}</Text>
            </View>
            <View
                style={{
                    borderBottomColor: '#eee',
                    borderBottomWidth: 2,
                    marginHorizontal: 10
                }}
            />
            <View style={styles.learnMoreContainer}>
                <Text style={styles.learnMore}>Learn More</Text>
            </View>

        </TouchableOpacity>
    );
}


const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width: width * (.6),
        height: height * (.33),
        marginTop: 10,
        marginRight: 10
    },

    imgContainer: {
        flex: 3
    },

    titleContainer: {
        flex: 2,
        paddingHorizontal: 10,
        alignContent: 'center',
        justifyContent: 'center',
    },

    title: {
        fontWeight: '900',
        fontSize: 20
    },

    headline: {
        fontSize: 15
    },

    learnMoreContainer: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'space-around',
    },

    learnMore: {
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15,
        color: '#41a5d7',
    }

});

export default newsList;
