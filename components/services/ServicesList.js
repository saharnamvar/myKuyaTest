import React, {useEffect, useState} from 'react';
import {LayoutAnimation, Platform, StyleSheet, TouchableWithoutFeedback, UIManager, View,} from 'react-native';

import IconFA from 'react-native-vector-icons/dist/FontAwesome';
import ServiceItem from './ServiceItem'


let imgDir = '../../images/';
const servicesListData = [
    {id: 1, title: 'title1', img: require(imgDir + '2-icon.png')},
    {id: 2, title: 'title2', img: require(imgDir + '3-icon.png')},
    {id: 3, title: 'title3', img: require(imgDir + '4-icon.png')},
    {id: 4, title: 'title4', img: require(imgDir + '6-icon.png')},
    {id: 5, title: 'title5', img: require(imgDir + '7-icon.png')},
    {id: 6, title: 'title6', img: require(imgDir + '8-icon.png')},
    {id: 7, title: 'title7', img: require(imgDir + '9-icon.png')},
    {id: 8, title: 'title8', img: require(imgDir + '10-icon.png')},
    {id: 9, title: 'title9', img: require(imgDir + '12-icon.png')},
    {id: 10, title: 'title10', img: require(imgDir + '13-icon.png')},
    {id: 11, title: 'title11', img: require(imgDir + '14-icon.png')},
    {id: 12, title: 'title12', img: require(imgDir + '15-icon.png')},
    {id: 13, title: 'title13', img: require(imgDir + '16-icon.png')},
    {id: 14, title: 'title14', img: require(imgDir + '17-icon.png')},
    {id: 15, title: 'title15', img: require(imgDir + '23-icon.png')},
    {id: 16, title: 'title16', img: require(imgDir + '24-icon.png')},
    {id: 17, title: 'title17', img: require(imgDir + '25-icon.png')},
    {id: 18, title: 'title18', img: require(imgDir + '26-icon.png')},
    {id: 19, title: 'title19', img: require(imgDir + '27-icon.png')},
    {id: 20, title: 'title20', img: require(imgDir + '28-icon.png')},
];

if (
    Platform.OS === 'android' &&
    UIManager.setLayoutAnimationEnabledExperimental
) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
}

const servicesList = () => {
    const initListCount = 6;

    const [servicesList, setServicesList] = useState([]);
    const [listCount, setListCount] = useState(initListCount);
    const [containerExpanded, setContainerExpanded] = useState(false);

    useEffect(() => {
        let servicesList = getServicesList();
        setServicesList(servicesList);
    }, []);

    function getServicesList() {
        return servicesListData;
    }

    function toggle() {
        let count = containerExpanded ? initListCount : servicesList.length;
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        setListCount(count);
        setContainerExpanded(!containerExpanded);
    }

    return (
        <View>

            {/*flatList should be used in case of large list that not surround with scrollView container*/}
            <View style={styles.itemContainer}>
                {servicesList.slice(0, listCount).map(item => {
                    return (
                        <ServiceItem item={item} key={item.id} columnsNum={3}/>
                    )
                })}
            </View>

            <TouchableWithoutFeedback
                onPress={toggle}
            >
                <View style={styles.iconContainer}>
                    <IconFA name={containerExpanded ? "chevron-up" : "chevron-down"}
                            size={25}
                            color={'darkgrey'}
                    />
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    itemContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignContent: 'center',
        justifyContent: 'space-around',
    },

    iconContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

});


export default servicesList;
