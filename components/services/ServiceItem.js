import React from 'react';
import {Image, Dimensions, StyleSheet, Text, TouchableOpacity} from 'react-native';


const serviceItem = (props) => {

    let columnsNum = props.columnsNum;
    return (
        <TouchableOpacity style={[styles.container, {width: (100/columnsNum)+"%"}]}>
            <Image style={[styles.imgContainer, {width: Dimensions.get('window').width * (0.5 / columnsNum),height: Dimensions.get('window').width * (0.5 / columnsNum)}]}
                   resizeMode='contain' source={props.item.img}/>
            <Text style={styles.title}>{props.item.title}</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 10
    },

    imgContainer: {
        opacity: 0.7,
        padding: 0
    },

    title: {
        textAlign: 'center',
    }
});

export default serviceItem;
