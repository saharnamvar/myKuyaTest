import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View,} from 'react-native';

import globalStyle from '../../styles/globalStyle';
import ServiceItem from './ServiceItem'

let imgDir = '../../images/';
const servicesListData = [
    {id: 1, title: 'title1', img: require(imgDir + '2-icon.png')},
    {id: 2, title: 'title2', img: require(imgDir + '3-icon.png')},
    {id: 3, title: 'title3', img: require(imgDir + '4-icon.png')},
];


const servicesList = () => {
    const [servicesList, setServicesList] = useState([]);

    useEffect(() => {
        let servicesList = getServicesList();
        setServicesList(servicesList);
    }, []);

    function getServicesList() {
        return servicesListData;
    }

    return (
        <View style={globalStyle.card}>
            <View style={styles.sectionHeaderContainer}>
                <Text style={globalStyle.sectionHeader}>Featured</Text>
            </View>
            <View
                style={{
                    borderBottomColor: '#eee',
                    borderBottomWidth: 2,
                    marginHorizontal: 10
                }}
            />
            <View style={styles.itemContainer}>
                {servicesList.map(item => {
                    return (
                        <ServiceItem item={item} key={item.id} columnsNum={3}/>
                    )
                })}
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    sectionHeaderContainer: {
        padding: 10,
    },

    itemContainer: {
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-around',
        flexWrap: 'wrap',
    },

});


export default servicesList;
