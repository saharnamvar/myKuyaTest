import React from "react";
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    card: {
        overflow: 'hidden',
        borderRadius: 7,
        shadowColor: '#ccc',
        shadowOffset: {width: 10, height: 10},
        shadowRadius: 50,
        elevation: 4,
        backgroundColor: '#fff',
        margin: 2,
    },

    customText: {
        textAlign: 'center'
    },

    sectionHeader:{
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default styles;