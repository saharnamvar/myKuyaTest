import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import home from '../screens/home';
import map from '../screens/map';

const BottomTabNavigator = createBottomTabNavigator({
    home,
    map
});

export default BottomTabNavigator;
