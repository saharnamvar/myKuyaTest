import React from 'react';
import Svg, {
    Circle,
    Ellipse,
    G,
    Text,
    TSpan,
    TextPath,
    Path,
    Polygon,
    Polyline,
    Line,
    Rect,
    Use,
    Image,
    Symbol,
    Defs,
    LinearGradient,
    RadialGradient,
    Stop,
    ClipPath,
    Pattern,
    Mask,
} from 'react-native-svg';

function getProperties(props) {
    let svgProp = {
        width: props.width || '100%',
        height: props.height || '100%',
        viewBox: props.viewBox || "0 0 100 100"
    };
    let fill = props.fill || "#000";

    return {svgProp, fill}
}

const Mail = (props) => {
    const properties = getProperties(props);
    return (

        <Svg  {...properties.svgProp}>
            <Path fill={properties.fill} d="M511.6,197.6c0-0.8-0.2-1.9-0.5-2.6c-0.8-2.9-2.5-5.5-5.2-7.3l-73.3-50.9V82.2c0-7.2-5.9-13.1-13.1-13.1h-84.3
	l-71.7-49.9c-4.5-3.1-10.4-3.1-14.9,0L176.4,69h-84c-7.2,0-13.1,5.9-13.1,13.1v54L5.7,187c-3.6,2.5-5.6,6.5-5.6,10.6
	c0,0.1,0,0.1,0,0.2l0.4,284.2c0,3.5,1.4,6.8,3.9,9.3c2.5,2.5,5.8,3.8,9.3,3.8c0,0,0,0,0,0l485.4-0.7c7.2,0,13.1-5.9,13.1-13.1
	L511.6,197.6z M432.7,168.7l41.9,29.1L432.7,227V168.7z M256,45.9L289.3,69h-66.8L256,45.9z M167.2,147.7h177.5
	c7.2,0,13.1,5.9,13.1,13.1c0,7.2-5.9,13.1-13.1,13.1H167.2c-7.2,0-13.1-5.9-13.1-13.1C154.1,153.6,160,147.7,167.2,147.7z
	 M167.2,215.9h177.5c7.2,0,13.1,5.9,13.1,13.1c0,7.2-5.9,13.1-13.1,13.1H167.2c-7.2,0-13.1-5.9-13.1-13.1
	C154.1,221.8,160,215.9,167.2,215.9z M79.3,168v59.6l-43.1-29.8L79.3,168z M26.3,222.9l158.7,109.7L26.6,455.3L26.3,222.9z
	 M51.9,468.9l204.3-158.5l203.6,157.9L51.9,468.9z M327.1,332.3l158.3-110l0.3,233.1L327.1,332.3z"/>
        </Svg>
    )
}


const Location = (props) => {

    const properties = getProperties(props);

    return (
        <Svg {...properties.svgProp} viewBox="11.8 -162.4 426.5 565.8">
            <G id="Layer_2">

                <RadialGradient id="SVGID_1_" cx="224.9995" cy="774.3544" r="65.0005"
                                gradientTransform="matrix(1 0 0 0.2964 0 148.874)" gradientUnits="userSpaceOnUse">
                    <Stop offset="0" style="stop-color:#000000;stop-opacity:0.4"/>
                    <Stop offset="1" style="stop-color:#000000;stop-opacity:0.4"/>
                </RadialGradient>
                <Ellipse className="st0" cx="225" cy="378.4" rx="65" ry="19.3"/>
            </G>
            <G id="Layer_1">
                <G>
                    <Path fill='#FFFFFF' d="M225,378.4c-10.6,0-20.6-3.5-28.2-9.7c-1.9-1.5-46.7-38.5-92-95.8C77.9,239,56.6,205.1,41.2,172
			C21.7,130,11.8,89.3,11.8,50.8c0-57,22.2-110.5,62.5-150.8c40.3-40.3,93.8-62.5,150.8-62.5c117.6,0,213.2,95.7,213.2,213.2
			c0,38.4-9.9,79.2-29.4,121.2c-15.4,33.1-36.8,67-63.6,100.8c-45.4,57.3-90.1,94.3-92,95.8C245.6,374.9,235.6,378.4,225,378.4z"/>
                    <Path fill='#DF5D4D' d="M423.8,50.8c0,159.2-179.8,306.7-179.8,306.7c-10.5,8.6-27.7,8.6-38.2,0c0,0-179.8-147.5-179.8-306.7
			C26.2-59,115.2-148,225-148S423.8-59,423.8,50.8z"/>
                    <Path fill='#FFFFFF' d="M225,165.3c-63.1,0-114.4-51.3-114.4-114.4c0-63.1,51.3-114.4,114.4-114.4c63.1,0,114.4,51.3,114.4,114.4
			C339.4,114,288.1,165.3,225,165.3z"/>
                    <Path display="none" fill='#FFFFFF'
                          d="M225,150.9c-55.2,0-100-44.9-100-100s44.9-100,100-100s100,44.9,100,100S280.2,150.9,225,150.9z"/>
                </G>
            </G>
        </Svg>
    )
}

export {Mail, Location}
